////  ViewController.swift
//  UITableViewWithCustomCellNoStoryboard
//
//  Created on 02/06/2020.
//  Copyright © 2020 kostowski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let tableView = UITableView()
    var videos: [Video] = []
    
    struct Cells {
        static let videoCell = "VideoCell"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        view.backgroundColor = .systemBlue
        title = "Pogoda Jacka"
        configureTableView()
        videos = fetchData()
    }
    
    func configureTableView() {
        view.addSubview(tableView)
        // set delegates
            setTableViewDelegates()
        // set row height
            tableView.rowHeight = 100
        // register cells
        tableView.register(VideoCell.self, forCellReuseIdentifier: Cells.videoCell)
        // set constrains
            tableView.pin(to: view)
    }
    
    func setTableViewDelegates() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cells.videoCell) as! VideoCell
        let video = videos[indexPath.row]
        cell.set(video: video)
        return cell
    }
}

extension ViewController {
    func fetchData() -> [Video] {
        let v1 = Video(image: Images.cloud, title: "Cloud")
        let v2 = Video(image: Images.cloudbolt, title: "Cloudbolt")
        let v3 = Video(image: Images.cloudfog, title: "Cloudfog")
        let v4 = Video(image: Images.moon, title: "Moon")
        let v5 = Video(image: Images.smoke, title: "Smoke")
        let v6 = Video(image: Images.sunhaze, title: "Sun")
        let v7 = Video(image: Images.sundust, title: "Sundust")
        let v8 = Video(image: Images.sunmax, title: "Sunmax")
        let v9 = Video(image: Images.sunrise, title: "Sunrise")
        let v10 = Video(image: Images.sunset, title: "Sunset")
        
        return [v1,v2,v3,v4,v5,v6,v7,v8,v9,v10]
    }
}
