////  Image.swift
//  UITableViewWithCustomCellNoStoryboard
//
//  Created on 02/06/2020.
//  Copyright © 2020 kostowski. All rights reserved.
//

import UIKit

struct Images {
    static let sunhaze          = UIImage(systemName: "sun.haze")!
    static let sunmax       = UIImage(systemName: "sun.max")!
    static let sunrise      = UIImage(systemName: "sunrise")!
    static let sunset       = UIImage(systemName: "sunset")!
    static let sundust      = UIImage(systemName: "sun.dust")!
    static let moon         = UIImage(systemName: "moon")!
    static let cloud        = UIImage(systemName: "cloud")!
    static let cloudfog     = UIImage(systemName: "cloud.fog")!
    static let cloudbolt    = UIImage(systemName: "cloud.bolt")!
    static let smoke        = UIImage(systemName: "smoke")!
}
