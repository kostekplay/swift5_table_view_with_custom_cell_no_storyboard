////  File.swift
//  UITableViewWithCustomCellNoStoryboard
//
//  Created on 02/06/2020.
//  Copyright © 2020 kostowski. All rights reserved.
//

import UIKit

extension UIView {
    func pin (to superView: UIView) {
        translatesAutoresizingMaskIntoConstraints                               = false
        topAnchor.constraint(equalTo: superView.topAnchor).isActive             = true
        leadingAnchor.constraint(equalTo: superView.leadingAnchor).isActive     = true
        trailingAnchor.constraint(equalTo: superView.trailingAnchor).isActive   = true
        bottomAnchor.constraint(equalTo: superView.bottomAnchor).isActive       = true
    }
}
