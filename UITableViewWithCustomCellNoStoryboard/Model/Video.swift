////  Video.swift
//  UITableViewWithCustomCellNoStoryboard
//
//  Created on 02/06/2020.
//  Copyright © 2020 kostowski. All rights reserved.
//

import UIKit

struct Video {
    var image: UIImage
    var title: String
}
